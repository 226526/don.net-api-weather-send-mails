﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HtmlAgilityPack;
using System.Net.Mail;
using System.ComponentModel;
using System.IO;
using System.Text.RegularExpressions;
using DownloadNodesSample;
using System.Reflection;
using System.Xml.Serialization;

namespace WpfApp2 
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 



    public partial class MainWindow : Window
    {
        List<String> wyniki=new List<string>();
        List<Base_b> baza_list = new List<Base_b>();

        string path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        string image = "0image.jpg";

        Base_taskEntities context = new Base_taskEntities();

        public MainWindow()
        {
            InitializeComponent();
            this.Width = 750;
            this.Height = 510;

            /*
            foreach (var task in context.Base_task) 
            {
                baza_list.Add(task);
            }
            foreach (var task in context.Base_weather) 
            {
                baza_list.Add(task);
            }

            baza_list.Sort((x, y) => DateTime.Compare(x.czas, y.czas));
            
            foreach (var task in baza_list)
            {
                if(task is Base_task)
                {
                 
                    ListBoxItem itm = new ListBoxItem();
                    itm.Content = task.give_text_task();
                    ListTask.Items.Add(itm);
                }

                if (task is Base_weather)
                {

                    ListBoxItem itm = new ListBoxItem();
                    itm.Content = task.give_text_task();
                    ListTask.Items.Add(itm);
                }

            }
            */
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (website_word.IsSelected)
            {
                using (var ctx = new Base_taskEntities())
                {
                    var task = new Base_task()
                    {
                        word = boxword.Text,
                        url = boxwebsite.Text,
                        emailfrom = boxMailFromName.Text,
                        passfrom = boxMailFromPass.Text,
                        emailto = boxMailToName.Text,
                        czas = DateTime.Now,
                    };
                    ctx.Base_task.Add(task);
                    baza_list.Add(task);
                    ctx.SaveChanges();

                }
                

                ListBoxItem itm = new ListBoxItem();
                itm.Content = "Znajdź słowo \"" + boxword.Text + "\" na stronie: " + boxwebsite.Text;
                ListTask.Items.Add(itm);

                ListTask.Focus();
                ListTask.SelectedIndex = ListTask.Items.Count - 1;
                ((ListBoxItem)ListTask.SelectedItem).Focus();
            }

            if(website_weather.IsSelected)
            {
                using (var ctx = new Base_taskEntities())
                {
                    var task = new Base_weather()
                    {
                        city = boxcity.Text,
                        weather = boxtemp.Text,
                        emailfrom = boxMailFromName.Text,
                        passfrom = boxMailFromPass.Text,
                        emailto = boxMailToName.Text,
                        czas = DateTime.Now,
                    };
                    ctx.Base_weather.Add(task);
                    baza_list.Add(task);
                    ctx.SaveChanges();
                }


                ListBoxItem itm = new ListBoxItem();
                itm.Content = "Sprawdz czy w miescie \"" + boxcity.Text + "\" jest powyzej " + boxtemp.Text;
                ListTask.Items.Add(itm);

                ListTask.Focus();
                ListTask.SelectedIndex = ListTask.Items.Count - 1;
                ((ListBoxItem)ListTask.SelectedItem).Focus();

            }

        }

        private void Czysc_Click(object sender, RoutedEventArgs e)
        {

            ListTask.Items.Clear();
            using (var context = new Base_taskEntities())
            {
                var itemsToDelete = context.Set<Base_task>();
                context.Base_task.RemoveRange(itemsToDelete);
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Base_task', RESEED, 0)");

                var itemsToDelete2 = context.Set<Base_weather>();
                context.Base_weather.RemoveRange(itemsToDelete2);
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Base_weather', RESEED, 0)");

                baza_list.Clear();

                context.SaveChanges();
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

            if (wordAndwebsite())
            {
                if (!ListTask.Items.IsEmpty&&baza_list.Count!=0)
                {
                    using (StreamWriter log = File.CreateText("log.log"))
                    {
                        log.WriteLine("Start programu");
                    }
                    
                    foreach (var task in baza_list)
                    {
                            task.do_my_task();
                    }
                    


                    /////////////////////////////////////////////////////
                    /*
                    foreach (var task in context.Base_task) 
                    {
                        task.do_my_task();
                        
                        var html = new HTML(task.url, task.word);
                        html.PrintPageNodes(wyniki);
                        foreach(string url in wyniki)
                        {
                            html.download(url, path, image);
                            E_mail.send_mail(task.emailfrom, task.passfrom, task.emailto, path, image);
                            html.delete(path, image);
                        }
                        wyniki.Clear();
                        
                    }
                    */
                    
                    
                    MessageBox.Show("Wysłano mail");
                }
                else
                {
                    MessageBox.Show("Lista jest pusta.");
                }
            }

        }

        private Boolean sendMailTrue()
        {
            if (boxMailFromNameEmpty(boxMailFromName) && boxMailFromPassEmpty(boxMailFromPass) && boxWMailToNameEmpty(boxMailToName)) //TODO jezeli nie ma co wyslac, nie wysylaj
            {
                return true;
            }
            return false;
        }

        private Boolean wordAndwebsite()
        {
            if (boxWebsiteEmpty(boxwebsite) && boxWordEmpty(boxword))
            {
                return true;
            }
            return false;

        }

        private Boolean boxWebsiteEmpty(TextBox text)
        {
            if (string.IsNullOrEmpty(text.Text))
            {
                MessageBox.Show("Podaj nazwe strony");
                return false;
            }
            else
            {
                return true;
            }

        }

        private Boolean boxWordEmpty(TextBox text)
        {
            if (string.IsNullOrEmpty(text.Text))
            {
                MessageBox.Show("Podaj szukane slowo");
                return false;
            }
            else
            {
                return true;
            }
        }

        private Boolean boxMailFromNameEmpty(TextBox text)
        {
            if (string.IsNullOrEmpty(text.Text))
            {
                MessageBox.Show("Podaj mail z ktorego chcesz wyslac");
                return false;
            }
            else
            {
                return true;
            }
        }

        private Boolean boxMailFromPassEmpty(TextBox text)
        {
            if (string.IsNullOrEmpty(text.Text))
            {
                MessageBox.Show("Podaj haslo maila z ktorego chcesz wyslac");
                return false;
            }
            else
            {
                return true;
            }
        }

        private Boolean boxWMailToNameEmpty(TextBox text)
        {
            if (string.IsNullOrEmpty(text.Text))
            {
                MessageBox.Show("Podaj maila na ktorego chcesz wyslac");
                return false;
            }
            else
            {
                return true;
            }
        }

        private void UsunButton_Click(object sender, RoutedEventArgs e)
        {
            if (ListTask.SelectedIndex > -1)
            {
                int d = ListTask.SelectedIndex;


                ListTask.Items.Remove(ListTask.SelectedItem);

                if (d > ListTask.Items.Count-1) d -= 1;
                if (d > -1)
                {
                    ListTask.Focus();
                    ListTask.SelectedIndex = d;
                    ((ListBoxItem)ListTask.SelectedItem).Focus();
                }
                
            }
            else
            {
                MessageBox.Show("Nie wybrano elementu");
            }
        }

        private void Seria_Click(object sender, RoutedEventArgs e)
        {
            string fileName = "xd.xml";
            using (FileStream stream = new FileStream(fileName, FileMode.Create))
            {
                XmlSerializer XML = new XmlSerializer(typeof(List<Base_b>));
                XML.Serialize(stream, baza_list);
            }
            MessageBox.Show("Zapisano");
        }

        private void Deseria_Click(object sender, RoutedEventArgs e)
        {
            string fileName = "xd.xml";
            using (FileStream stream = new FileStream(fileName, FileMode.Open))
            {
                XmlSerializer XML = new XmlSerializer(typeof(List<Base_b>));
                baza_list = (List<Base_b>)XML.Deserialize(stream);
            }
            foreach (var task in baza_list)
            {
                if (task is Base_task)
                {

                    ListBoxItem itm = new ListBoxItem();
                    itm.Content = task.give_text_task();
                    ListTask.Items.Add(itm);
                }

                if (task is Base_weather)
                {

                    ListBoxItem itm = new ListBoxItem();
                    itm.Content = task.give_text_task();
                    ListTask.Items.Add(itm);
                }

            }
            MessageBox.Show("Wczytano");
        }

        private void ListTask_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (website_word.IsSelected)
            {
                Console.WriteLine("jeststrona");
            }
        }

        private void checkWeatherButton_Click(object sender, RoutedEventArgs e)
        {
            Weather_form Weather_form = new Weather_form();
            Weather_form.Show();
        }
    }

}
