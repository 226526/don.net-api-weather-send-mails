﻿namespace WpfApp2
{
    partial class Weather_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Miasto_textBox = new System.Windows.Forms.TextBox();
            this.Sprawdz_pogode_button = new System.Windows.Forms.Button();
            this.Opis_Pogody_richTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Miasto:";
            // 
            // Miasto_textBox
            // 
            this.Miasto_textBox.Location = new System.Drawing.Point(73, 32);
            this.Miasto_textBox.Name = "Miasto_textBox";
            this.Miasto_textBox.Size = new System.Drawing.Size(100, 20);
            this.Miasto_textBox.TabIndex = 1;
            this.Miasto_textBox.Text = "Wrocław";
            // 
            // Sprawdz_pogode_button
            // 
            this.Sprawdz_pogode_button.Location = new System.Drawing.Point(29, 58);
            this.Sprawdz_pogode_button.Name = "Sprawdz_pogode_button";
            this.Sprawdz_pogode_button.Size = new System.Drawing.Size(100, 50);
            this.Sprawdz_pogode_button.TabIndex = 2;
            this.Sprawdz_pogode_button.Text = "Sprawdź pogodę";
            this.Sprawdz_pogode_button.UseVisualStyleBackColor = true;
            this.Sprawdz_pogode_button.Click += new System.EventHandler(this.Sprawdz_pogode_button_Click);
            // 
            // Opis_Pogody_richTextBox
            // 
            this.Opis_Pogody_richTextBox.Location = new System.Drawing.Point(29, 131);
            this.Opis_Pogody_richTextBox.Name = "Opis_Pogody_richTextBox";
            this.Opis_Pogody_richTextBox.Size = new System.Drawing.Size(229, 59);
            this.Opis_Pogody_richTextBox.TabIndex = 3;
            this.Opis_Pogody_richTextBox.Text = "";
            // 
            // Weather_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.Opis_Pogody_richTextBox);
            this.Controls.Add(this.Sprawdz_pogode_button);
            this.Controls.Add(this.Miasto_textBox);
            this.Controls.Add(this.label1);
            this.Name = "Weather_form";
            this.Text = "Pogoda";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Miasto_textBox;
        private System.Windows.Forms.Button Sprawdz_pogode_button;
        private System.Windows.Forms.RichTextBox Opis_Pogody_richTextBox;
    }
}