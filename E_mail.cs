﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net.Mime;
using DownloadNodesSample;
using HtmlAgilityPack;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using System.ComponentModel;
using System.Windows;

namespace WpfApp2
{
    class E_mail
    {
        public static void send_mail(string emailfrom, string passwordfrom, string emailto,string path, string image)
        {

            MailMessage mail;
            SmtpClient SmtpServer;
            mail = new MailMessage();
            SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            mail.From = new MailAddress(emailfrom);
            mail.To.Add(emailto);
            mail.Subject = "Powiadomienie o nowym obrazku";



            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment(path + "\\" + image);
            mail.Attachments.Add(attachment);

            SmtpServer.Credentials = new System.Net.NetworkCredential(emailfrom.Substring(0, emailfrom.Length - 10), passwordfrom);
            SmtpServer.EnableSsl = true;
            SmtpServer.Send(mail);
            using (StreamWriter log = File.AppendText("log.log"))
            {
                log.WriteLine("wyslalem email z " + path + "\\" + image);
            }
            attachment.Dispose();
            using (StreamWriter log = File.AppendText("log.log"))
            {
                log.WriteLine("usunalem plik " + path + "\\" + image);
            }
        }

        public static void send_mail(string emailfrom, string passwordfrom, string emailto, string message)
        {
            MailMessage mail;
            SmtpClient SmtpServer;
            mail = new MailMessage();
            SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            mail.From = new MailAddress(emailfrom);
            mail.To.Add(emailto);
            mail.Subject = "Powiadomienie o obecnej temperaturze";
            mail.Body = message;
            SmtpServer.Credentials = new System.Net.NetworkCredential(emailfrom.Substring(0, emailfrom.Length - 10), passwordfrom);
            SmtpServer.EnableSsl = true;
            SmtpServer.Send(mail);



        }


        }
}
