﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using WpfApp2;

namespace DownloadNodesSample
{
    public class HTML
    {
        private readonly string _url;
        private readonly string _word;

        public HTML(string url)
        {
            this._url = url;
        }

        public HTML(string url, string word)
        {
            this._url = url;
            this._word = word;
        }

        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(_url));

                return html;
            }
        }

        public void PrintPageNodes(List<String> result)
        {

            var doc = new HtmlDocument();

            var pageHtml = GetPageHtml();

            doc.LoadHtml(pageHtml);

            var nodes = doc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                if (node.GetAttributeValue("alt", "").Contains(_word))
                {
                    result.Add(node.GetAttributeValue("src", ""));
                }

            }

        }

        public void download(string word,string path,string image)
        {
            using (var client = new WebClient())
            {
                client.DownloadFile(word, path + "\\" + image);
            }
        }


        public void delete(string path, string image)
        {
            using (var client = new WebClient())
            {
                if (File.Exists(path + "\\" + image))
                {
                    File.Delete(path + "\\" + image); 
                }
            }
        }

        public string download_json()
        {
            using (WebClient wc = new WebClient())
            {
                var json = @wc.DownloadString(this._url);
                return json;
            }
        }



    }
}