﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DownloadNodesSample;
using Newtonsoft.Json;
using System.Net;

namespace WpfApp2
{
    

    public partial class Weather_form : Form
    {
       

        public Weather_form()
        {
            InitializeComponent();
        }

        

        private void Sprawdz_pogode_button_Click(object sender, EventArgs e)
        {
            //string json="";
            string city = this.Miasto_textBox.Text;
            if (city == "")
            {
                MessageBox.Show("Uzupełnij nazwę miasta");
            }
            else
            {
                string key = "1501a0987eacc803eced7719c7b9ad9f";
                string str = string.Format("http://api.openweathermap.org/data/2.5/weather?appid=" + key + "&q=" + city);
                string message = "";
                double temp_now = 0;
                double temp_max = 0;
                double temp_min = 0;
                double pressure = 0;
                string description;
                var html = new HTML(str);
                using (WebClient wc = new WebClient())
                {
                    var json = @wc.DownloadString(str);


                    var result = JsonConvert.DeserializeObject<RootObject>(json);



                    temp_now = Math.Round((double)result.main.temp - 273.15, 2);
                    pressure = result.main.pressure;
                    temp_max = (double)result.main.temp_max - 273.15;
                    temp_min = (double)result.main.temp_min - 273.15;
                    description = result.weather[0].description;
                }

                message = "It's " + temp_now + "°C hot (from " + temp_min + "°C to " + temp_max + "°C).\nPressure " + pressure + " hPa.\nWhat's the weather like? " + description;
                Opis_Pogody_richTextBox.Text = message;
            }
        }
    }
}
